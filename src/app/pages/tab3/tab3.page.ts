import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  
  platforms = [];

  constructor(private dataService: DataService) {}

  loadPlatforms(event?) {
    this.dataService.getPlatforms().subscribe(
      resp => {
        console.log('Platforms', resp);
        if(resp.next === null){
          this.platforms.push(...resp.results);
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.platforms.push(...resp.results);
        console.log(resp.next);
        if(event){
          event.target.complete();
        }
      }
    );
  }

  loadData(event) {
    this.loadPlatforms(event);
  }

  ngOnInit() {
    this.loadPlatforms();
  }

}
