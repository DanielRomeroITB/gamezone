import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-genre',
  templateUrl: './detalle-genre.component.html',
  styleUrls: ['./detalle-genre.component.scss'],
})
export class DetalleGenreComponent implements OnInit {

  @Input() id;

  genre;

  constructor(private modalCtrl: ModalController, private dataService: DataService) { }

  loadDetalleGenre() {
    this.dataService.getGenreDetall(this.id).subscribe(
      resp => {
        console.log('Genre', resp);
        this.genre = resp;
      }
    );
  }


  ngOnInit() {
    this.loadDetalleGenre();
  }

  regresar(){
    this.modalCtrl.dismiss()
  }
 
}
