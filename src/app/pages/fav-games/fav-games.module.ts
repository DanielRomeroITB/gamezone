import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FavGamesPageRoutingModule } from './fav-games-routing.module';

import { FavGamesPage } from './fav-games.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FavGamesPageRoutingModule
  ],
  declarations: [FavGamesPage]
})
export class FavGamesPageModule {}
