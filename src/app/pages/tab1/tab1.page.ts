import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Game } from '../../interfaces/interfaces';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  games = [];
  searched = false; 

  constructor(private dataService: DataService, public router: Router) {}

  loadGames(event?) {
    this.dataService.getGames().subscribe(
      resp => {
        console.log('Games', resp);
        if(resp.next === null){
          this.games.push(...resp.results);
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.games.push(...resp.results);
        console.log(resp.next);
        if(event){
          event.target.complete();
        }
      }
    );
  }

  getSearchGames(event) {
    this.dataService.getGamesSearch(event.target.value).subscribe(
      resp => {
        this.games = [];
        console.log('Games Search', resp);
        this.games.push(...resp.results);
      }
    );
  }

  favourites() {
    this.router.navigateByUrl('fav-games');
  }

  loadData(event) {
    this.loadGames(event);
  }

  setFocus() {
    this.searched = true;
  }

  clear() {
    this.searched = false;
  }

  blockScroll(event) {
    event.target.complete();
  }

  ngOnInit() {
    this.loadGames();
  }
}
