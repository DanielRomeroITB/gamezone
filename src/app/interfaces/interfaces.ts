// Interfaces juegos

export interface RespuestaGames {
  count: number;
  next: string;
  previous?: any;
  results: Game[];
}

export interface Game {
  id: number;
  name: string;
  released: string;
  background_image: string;
  rating: number;
}


// Interfaces Genero

export interface RespuestaGenre {
  count: number;
  next?: any;
  previous?: any;
  results: Genre[];
}

export interface Genre {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
  games: GamesGenre[];
}

export interface GamesGenre {
  id: number;
  slug: string;
  name: string;
  added: number;
}


// Interfaces Plataformas

export interface RespuestaPlatform {
  count: number;
  next: string;
  previous?: any;
  results: Platform[];
}

export interface Platform {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
  image?: any;
  year_start?: number;
  year_end?: any;
  games: GamesPlatform[];
}

export interface GamesPlatform {
  id: number;
  slug: string;
  name: string;
  added: number;
}


//Interfaces detalles juegos

export interface RespuestaDetallGame {
  id: number;
  name: string;
  description_raw: string;
  metacritic: number;
  released: string;
  tba: boolean;
  background_image: string;
  rating: number;
  parent_platforms: DetalleGamesPlatforms[];
  genres: DetalleGamesGenre[];
}

export interface DetalleGamesGenre {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
}

export interface DetalleGamesPlatforms {
  platform: DetalleGamesPlatform;
}

export interface DetalleGamesPlatform {
  id: number;
  name: string;
  slug: string;
}


// Detalles Genre

export interface RespuestaDetallGenre {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
  description: string;
}


// Detalles Platform

export interface RespuestaDetallPlatform {
  id: number;
  name: string;
  slug: string;
  games_count: number;
  image_background: string;
  description: string;
  image?: any;
  year_start?: any;
  year_end?: any;
}