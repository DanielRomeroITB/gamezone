import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  genres = [];

  constructor(private dataService: DataService) {}

  loadGenres() {
    this.dataService.getGenres().subscribe(
      resp => {
        console.log('Genres', resp);
        this.genres.push(...resp.results);
      }
    );
  }

  ngOnInit() {
    this.loadGenres();
  }

}
