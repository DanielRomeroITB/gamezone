import { Component, OnInit } from '@angular/core';
import { DataLocalService } from '../../services/data-local.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { DetalleGamesComponent } from 'src/app/components/detalle-games/detalle-games.component';

@Component({
  selector: 'app-fav-games',
  templateUrl: './fav-games.page.html',
  styleUrls: ['./fav-games.page.scss'],
})
export class FavGamesPage implements OnInit {

  favourites = [];
  constructor(private dataLocalService: DataLocalService, public router: Router ,private modalCtrl: ModalController) { }

  ngOnInit() {
    this.dataLocalService.getGame().then(data => {
      this.favourites = data;
    });
  }

  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetalleGamesComponent,
      componentProps: { id }
    })
    modal.present()
  }

  regresar(){
    this.router.navigateByUrl('tabs');
  }

  deleteFav(game) {
    this.dataLocalService.deleteGame(game);
    
    setTimeout(() => {
      this.dataLocalService.getGame().then(data => {
        this.favourites = data;
      });
    }, 45);
    
  }

}
