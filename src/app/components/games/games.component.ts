import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Game } from 'src/app/interfaces/interfaces';
import { DetalleGamesComponent } from '../detalle-games/detalle-games.component';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss'],
})
export class GamesComponent implements OnInit {

  @Input() games: Game[] = [];
  constructor(private modalCtrl: ModalController, private dataLocalService: DataLocalService) { }

  ngOnInit() {}

  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetalleGamesComponent,
      componentProps: { id }
    })
    modal.present()
  }
 
  addFav(game) {
    this.dataLocalService.setGame(game);
    console.log(game);
  }
}
