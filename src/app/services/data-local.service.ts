import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { DetalleGamesComponent } from '../components/detalle-games/detalle-games.component';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  private _storage: Storage | null = null;

  gamesFav: DetalleGamesComponent[] = [];

  constructor(private storage: Storage) {
    this.init()
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
  }

  async setGame(game) {
    this.gamesFav = await this.storage.get('game');
    
    let count = 0;
    this.gamesFav.forEach(gameFor => {
      if (game.id == gameFor.id) {
        count++;
      }
    });

    if (count == 1) {
      console.log('Game ya existe');
    } else {
      this.gamesFav.push(game);
      this.storage.set('game', this.gamesFav)
    }
  }

  deleteGame(game) {
    this.gamesFav = this.gamesFav.filter(gameFav=>gameFav.id!=game.id)
    this.storage.set('game', this.gamesFav)
  }

  async getGame() {
    this.gamesFav = await this.storage.get('game');
    return this.gamesFav;
  }
}
