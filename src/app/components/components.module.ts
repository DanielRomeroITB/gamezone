import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GamesComponent } from './games/games.component';
import { IonicModule } from '@ionic/angular';
import { GenresComponent } from './genres/genres.component';
import { PlatformsComponent } from './platforms/platforms.component';
import { DetalleGamesComponent } from './detalle-games/detalle-games.component';
import { DetalleGenreComponent } from './detalle-genre/detalle-genre.component';
import { DetallePlatformComponent } from './detalle-platform/detalle-platform.component';



@NgModule({
  declarations: [
    GamesComponent,
    GenresComponent,
    PlatformsComponent,
    DetalleGamesComponent,
    DetalleGenreComponent,
    DetallePlatformComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    GamesComponent,
    GenresComponent,
    PlatformsComponent,
    DetalleGamesComponent,
    DetalleGenreComponent,
    DetallePlatformComponent
  ]
})
export class ComponentsModule { }
