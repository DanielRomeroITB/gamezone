import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RespuestaGames, RespuestaGenre, RespuestaPlatform, RespuestaDetallGame, RespuestaDetallGenre, RespuestaDetallPlatform } from '../interfaces/interfaces';
import { environment } from '../../environments/environment';

const URL = environment.url;
const APIKEY = environment.apikey;

@Injectable({
  providedIn: 'root'
})
export class DataService {

  gamePage:number = 0;
  gameSearchPage:number = 0;
  platformPage:number = 0;

  constructor(private http: HttpClient) { }

  getGames() {
    this.gamePage++;
    return this.http.get<RespuestaGames>(`${URL}/games?${APIKEY}&page=${this.gamePage}`);
  }
  
  getPlatforms() {
    this.platformPage++;
    return this.http.get<RespuestaPlatform>(`${URL}/platforms?${APIKEY}&page=${this.platformPage}`);
  }

  getGenres() {
    return this.http.get<RespuestaGenre>(`${URL}/genres?${APIKEY}`);
  }

  getGamesDetall(id){
    return this.http.get<RespuestaDetallGame>(`${URL}/games/${id}?${APIKEY}`);
  }

  getGenreDetall(id){
    return this.http.get<RespuestaDetallGenre>(`${URL}/genres/${id}?${APIKEY}`);
  }

  getPlatformDetall(id){
    return this.http.get<RespuestaDetallPlatform>(`${URL}/platforms/${id}?${APIKEY}`);
  }

  getGamesSearch(input) {
    this.gamePage = 1;
    return this.http.get<RespuestaGames>(`${URL}/games?${APIKEY}&search=${input}`);
  }
}
