import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle-platform',
  templateUrl: './detalle-platform.component.html',
  styleUrls: ['./detalle-platform.component.scss'],
})
export class DetallePlatformComponent implements OnInit {

  @Input() id;

  platform;

  constructor(private modalCtrl: ModalController, private dataService: DataService) { }

  loadDetallePlatform() {
    this.dataService.getPlatformDetall(this.id).subscribe(
      resp => {
        console.log('Platform', resp);
        this.platform = resp;
      }
    );
  }


  ngOnInit() {
    this.loadDetallePlatform();
  }

  regresar(){
    this.modalCtrl.dismiss()
  }
}
