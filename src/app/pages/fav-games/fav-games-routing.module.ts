import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FavGamesPage } from './fav-games.page';

const routes: Routes = [
  {
    path: '',
    component: FavGamesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FavGamesPageRoutingModule {}
