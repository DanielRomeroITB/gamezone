import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Genre } from '../../interfaces/interfaces';
import { DetalleGenreComponent } from '../detalle-genre/detalle-genre.component';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss'],
})
export class GenresComponent implements OnInit {
  
  @Input() genres: Genre[] = [];

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetalleGenreComponent,
      componentProps: { id }
    })
    modal.present()
  }
 
}
