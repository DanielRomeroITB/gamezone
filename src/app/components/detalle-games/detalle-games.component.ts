import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Game } from '../../interfaces/interfaces';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-detalle-games',
  templateUrl: './detalle-games.component.html',
  styleUrls: ['./detalle-games.component.scss'],
})
export class DetalleGamesComponent implements OnInit {

  @Input() id;

  game;

  constructor(private modalCtrl: ModalController, private dataService: DataService, private dataLocalService : DataLocalService) { }

  loadDetalleGame() {
    this.dataService.getGamesDetall(this.id).subscribe(
      resp => {
        console.log('Game', resp);
        this.game = resp;
      }
    );
  }


  ngOnInit() {
    this.loadDetalleGame();
  }

  regresar(){
    this.modalCtrl.dismiss()
  }

   
  addFav(game) {
    this.dataLocalService.setGame(game);
    console.log(game);
  }
 

}
