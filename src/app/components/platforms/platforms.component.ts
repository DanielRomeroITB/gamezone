import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Platform } from '../../interfaces/interfaces';
import { DetallePlatformComponent } from '../detalle-platform/detalle-platform.component';

@Component({
  selector: 'app-platforms',
  templateUrl: './platforms.component.html',
  styleUrls: ['./platforms.component.scss'],
})
export class PlatformsComponent implements OnInit {

  @Input() platforms: Platform[] = [];

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  async verDetalle(id: string){
    const modal = await this.modalCtrl.create({
      component: DetallePlatformComponent,
      componentProps: { id }
    })
    modal.present()
  }
}
